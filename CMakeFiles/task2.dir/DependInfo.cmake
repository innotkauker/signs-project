# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/inni/signs-project/liblinear-1.93/blas/daxpy.c" "/home/inni/signs-project/CMakeFiles/task2.dir/liblinear-1.93/blas/daxpy.c.o"
  "/home/inni/signs-project/liblinear-1.93/blas/ddot.c" "/home/inni/signs-project/CMakeFiles/task2.dir/liblinear-1.93/blas/ddot.c.o"
  "/home/inni/signs-project/liblinear-1.93/blas/dnrm2.c" "/home/inni/signs-project/CMakeFiles/task2.dir/liblinear-1.93/blas/dnrm2.c.o"
  "/home/inni/signs-project/liblinear-1.93/blas/dscal.c" "/home/inni/signs-project/CMakeFiles/task2.dir/liblinear-1.93/blas/dscal.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/inni/signs-project/EasyBMP/EasyBMP.cpp" "/home/inni/signs-project/CMakeFiles/task2.dir/EasyBMP/EasyBMP.cpp.o"
  "/home/inni/signs-project/argvparser/argvparser.cpp" "/home/inni/signs-project/CMakeFiles/task2.dir/argvparser/argvparser.cpp.o"
  "/home/inni/signs-project/functions.cpp" "/home/inni/signs-project/CMakeFiles/task2.dir/functions.cpp.o"
  "/home/inni/signs-project/liblinear-1.93/linear.cpp" "/home/inni/signs-project/CMakeFiles/task2.dir/liblinear-1.93/linear.cpp.o"
  "/home/inni/signs-project/liblinear-1.93/tron.cpp" "/home/inni/signs-project/CMakeFiles/task2.dir/liblinear-1.93/tron.cpp.o"
  "/home/inni/signs-project/main.cpp" "/home/inni/signs-project/CMakeFiles/task2.dir/main.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
